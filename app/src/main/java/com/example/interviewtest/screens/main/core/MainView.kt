package com.example.interviewtest.screens.main.core

import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.interviewtest.R
import com.example.interviewtest.api.dto.UserModel
import com.example.interviewtest.databinding.ActivityMainBinding
import com.example.interviewtest.screens.main.MainActivity
import com.example.interviewtest.screens.main.adapters.UserAdapter
import io.reactivex.subjects.PublishSubject


class MainView(private var activity: MainActivity) {

    val subjects = PublishSubject.create<Int>()
    var binding: ActivityMainBinding = DataBindingUtil.setContentView(activity, R.layout.activity_main)

    fun fillRecycler(users: List<UserModel>){
        binding.userListRecyclerView.setHasFixedSize(true)
        binding.userListRecyclerView.layoutManager = LinearLayoutManager(activity)
        val dividerItemDecoration = DividerItemDecoration(activity, LinearLayoutManager(activity).orientation)
        binding.userListRecyclerView.addItemDecoration(dividerItemDecoration)
        val adapter = UserAdapter(users, activity)
        binding.userListRecyclerView.adapter = adapter
    }
}