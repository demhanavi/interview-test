package com.example.interviewtest.screens.login.dagger

import com.example.interviewtest.api.TestApi
import com.example.interviewtest.persistence.AppDatabase
import com.example.interviewtest.screens.login.LoginActivity
import com.example.interviewtest.screens.login.core.LoginModel
import com.example.interviewtest.screens.login.core.LoginPresenter
import com.example.interviewtest.screens.login.core.LoginView
import com.example.interviewtest.utils.rx.RxSchedulers
import dagger.Module
import dagger.Provides

@Module
class LoginModule(private val context: LoginActivity) {

    @LoginScope
    @Provides
    fun provideContext() = context

    @LoginScope
    @Provides
    fun provideView(context: LoginActivity) = LoginView(context)

    @LoginScope
    @Provides
    fun providePresenter(
        context: LoginActivity,
        view: LoginView,
        model: LoginModel,
        schedulers: RxSchedulers) = LoginPresenter(context, view, model, schedulers)

    @LoginScope
    @Provides
    fun provideModel(api: TestApi, appDatabase: AppDatabase) = LoginModel(api, appDatabase)
}