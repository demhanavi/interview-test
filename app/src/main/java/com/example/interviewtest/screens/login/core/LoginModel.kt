package com.example.interviewtest.screens.login.core

import com.example.interviewtest.api.TestApi
import com.example.interviewtest.api.dto.LoginRequest
import com.example.interviewtest.persistence.AppDatabase
import com.example.interviewtest.persistence.entities.User

class LoginModel(private val api: TestApi, private val appDatabase: AppDatabase) {

    fun doLogin(request: LoginRequest) = api.doLogin(request)

    fun saveUser(user: User) = appDatabase.userDao().insertUser(user)
}