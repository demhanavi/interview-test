package com.example.interviewtest.screens.login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.interviewtest.application.AppController
import com.example.interviewtest.screens.login.core.LoginModel
import com.example.interviewtest.screens.login.core.LoginPresenter
import com.example.interviewtest.screens.login.core.LoginView
import com.example.interviewtest.screens.login.dagger.DaggerLoginComponent
import com.example.interviewtest.screens.login.dagger.LoginModule
import com.example.interviewtest.screens.main.MainActivity
import javax.inject.Inject

class LoginActivity : AppCompatActivity() {

    @Inject
    lateinit var presenter: LoginPresenter
    @Inject
    lateinit var model: LoginModel
    @Inject
    lateinit var view: LoginView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerLoginComponent
            .builder()
            .appComponent(AppController.component)
            .loginModule(LoginModule(this))
            .build()
            .inject(this)
    }

    fun goToMain(){
        val main = Intent(this, MainActivity::class.java)
        startActivity(main)
        finish()
    }
}