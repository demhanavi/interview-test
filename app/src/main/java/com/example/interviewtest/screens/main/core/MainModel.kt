package com.example.interviewtest.screens.main.core

import com.example.interviewtest.api.TestApi
import com.example.interviewtest.persistence.AppDatabase

class MainModel(private val api: TestApi) {

    fun getUsers() = api.getUsers()
}