package com.example.interviewtest.screens.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.interviewtest.R
import com.example.interviewtest.application.AppController
import com.example.interviewtest.screens.main.core.MainModel
import com.example.interviewtest.screens.main.core.MainPresenter
import com.example.interviewtest.screens.main.core.MainView
import com.example.interviewtest.screens.main.dagger.DaggerMainComponent
import com.example.interviewtest.screens.main.dagger.MainModule
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var presenter: MainPresenter
    @Inject
    lateinit var model: MainModel
    @Inject
    lateinit var view: MainView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerMainComponent
            .builder()
            .appComponent(AppController.component)
            .mainModule(MainModule(this))
            .build()
            .inject(this)
    }
}