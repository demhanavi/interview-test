package com.example.interviewtest.screens.main.dagger

import com.example.interviewtest.api.TestApi
import com.example.interviewtest.screens.main.MainActivity
import com.example.interviewtest.screens.main.core.MainModel
import com.example.interviewtest.screens.main.core.MainPresenter
import com.example.interviewtest.screens.main.core.MainView
import com.example.interviewtest.utils.rx.RxSchedulers
import dagger.Module
import dagger.Provides

@Module
class MainModule(private val context: MainActivity) {

    @MainScope
    @Provides
    fun provideContext() = context

    @MainScope
    @Provides
    fun provideView(context: MainActivity) = MainView(context)

    @MainScope
    @Provides
    fun providePresenter(
        context: MainActivity,
        view: MainView,
        model: MainModel,
        schedulers: RxSchedulers) = MainPresenter(context, view, model, schedulers)

    @MainScope
    @Provides
    fun provideModel(api: TestApi) = MainModel(api)
}