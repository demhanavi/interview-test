package com.example.interviewtest.screens.main.core

import android.util.Log
import com.example.interviewtest.screens.main.MainActivity
import com.example.interviewtest.utils.rx.RxSchedulers
import io.reactivex.disposables.CompositeDisposable

class MainPresenter(
    private val activity: MainActivity,
    private val view: MainView,
    private val model: MainModel,
    private val schedulers: RxSchedulers) {

    private val subscriptions = CompositeDisposable()

    init {
        getUsers()
    }

    private fun getUsers(){
        subscriptions.add(model.getUsers()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.androidThread())
            .subscribe(
                {
                    view.fillRecycler(it.resources)
                },
                {
                    Log.e(MainPresenter::class.java.simpleName, "on Error Login ${it.message}")
                    it.printStackTrace()
                }
            ))
    }
}