package com.example.interviewtest.screens.login.dagger

import com.example.interviewtest.application.builder.AppComponent
import com.example.interviewtest.screens.login.LoginActivity
import dagger.Component

@LoginScope
@Component(modules = [LoginModule::class], dependencies = [AppComponent::class])
interface LoginComponent {
    fun inject(context: LoginActivity)
}