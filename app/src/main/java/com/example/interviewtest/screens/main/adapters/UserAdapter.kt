package com.example.interviewtest.screens.main.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.interviewtest.R
import com.example.interviewtest.api.dto.UserModel
import kotlinx.android.synthetic.main.user_item.view.*

class UserAdapter(val users: List<UserModel>, val context: Context): RecyclerView.Adapter<UserAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.user_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = users.get(position)
        holder.bind(item, context)
    }

    override fun getItemCount() = users.size

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        val email = itemView.email_text_view
        var username = itemView.user_name_text_view

        fun bind(item: UserModel, context: Context){
            email.text = item.email
            username.text = item.username

        }
    }
}