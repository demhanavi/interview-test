package com.example.interviewtest.screens.main.dagger

import com.example.interviewtest.application.builder.AppComponent
import com.example.interviewtest.screens.main.MainActivity
import dagger.Component

@MainScope
@Component(modules = [MainModule::class], dependencies = [AppComponent::class])
interface MainComponent {
    fun inject(context: MainActivity)
}