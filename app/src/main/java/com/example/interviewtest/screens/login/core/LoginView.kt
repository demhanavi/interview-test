package com.example.interviewtest.screens.login.core

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.interviewtest.R
import com.example.interviewtest.databinding.ActivityLoginBinding
import com.example.interviewtest.screens.login.LoginActivity
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class LoginView(private var activity: LoginActivity) {

    val subjects = PublishSubject.create<Int>()
    var binding: ActivityLoginBinding = DataBindingUtil.setContentView(activity, R.layout.activity_login)

    init {
        binding.loginButton.setOnClickListener {
            subjects.onNext(it.id)
        }
    }

    fun getClickSubjects(): Observable<Int> = subjects

    fun getEmail() : String{
        return if(binding.userNameEditText.text != null) {
            binding.userNameEditText.text.toString()
        } else{
            ""
        }
    }

    fun getPassword():String{
        return if(binding.passwordEditText.text != null) {
            binding.passwordEditText.text.toString()
        } else{
            ""
        }
    }

    fun successLogin(){
        activity.runOnUiThread {
            Toast.makeText(activity.applicationContext, "Success Login", Toast.LENGTH_LONG).show()
        }
    }

    fun unSuccessLogin(){
        activity.runOnUiThread {
            Toast.makeText(activity.applicationContext, "No Login", Toast.LENGTH_LONG).show()
        }
    }
}