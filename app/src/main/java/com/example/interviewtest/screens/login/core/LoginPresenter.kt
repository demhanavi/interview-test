package com.example.interviewtest.screens.login.core

import android.util.Log
import android.widget.Toast
import com.example.interviewtest.R
import com.example.interviewtest.api.dto.LoginRequest
import com.example.interviewtest.persistence.entities.User
import com.example.interviewtest.screens.login.LoginActivity
import com.example.interviewtest.utils.rx.RxSchedulers
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginPresenter(
    private val activity: LoginActivity,
    private val view: LoginView,
    private val model: LoginModel,
    private val schedulers: RxSchedulers) {

    private val subscriptions = CompositeDisposable()
    private lateinit var request: LoginRequest

    init {
        subscriptions.add(handleClicks())
    }

    private fun handleClicks(): Disposable {
        return view.getClickSubjects().subscribe {
            if (it == view.binding.loginButton.id) {
                doLogin()
            }
        }
    }

    private fun doLogin(){
        gatherUserInfo()
        subscriptions.add(model.doLogin(request)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.androidThread())
            .subscribe(
                {
                    Log.d(LoginPresenter::class.java.simpleName, "On Next Login")
                    GlobalScope.launch {
                        saveSession(it)
                        view.successLogin()
                        activity.goToMain()
                    }
                },
                {
                    Log.e(LoginPresenter::class.java.simpleName, "on Error Login ${it.message}")
                    it.printStackTrace()
                    view.unSuccessLogin()
                }))
    }

    private suspend fun saveSession(user: User) = withContext(Dispatchers.IO){
        model.saveUser(user)
    }

    private fun gatherUserInfo(){
        request = LoginRequest(view.getEmail(), view.getPassword())
    }
}