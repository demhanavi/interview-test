package com.example.interviewtest.application

import android.app.Application
import com.example.interviewtest.application.builder.AppComponent
import com.example.interviewtest.application.builder.DaggerAppComponent
import com.example.interviewtest.application.builder.modules.AppContextModule

class AppController: Application() {
    companion object{
        lateinit var component : AppComponent
    }


    override fun onCreate() {
        super.onCreate()
        initAppComponent()
    }

    private fun initAppComponent(){
        component = DaggerAppComponent.builder().appContextModule(AppContextModule(this)).build()
    }
}