package com.example.interviewtest.application.builder.modules.api

import com.example.interviewtest.api.TestApi
import com.example.interviewtest.application.builder.AppScope
import com.example.interviewtest.utils.PropertiesReader
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class TestApiModule {

    @AppScope
    @Provides
    fun provideTestApi(propertiesReader: PropertiesReader, client: OkHttpClient, converterFactory: GsonConverterFactory, adapter: RxJava2CallAdapterFactory) =
        Retrofit.Builder()
            .client(client)
            .baseUrl(propertiesReader.getBaseUrl())
            .addConverterFactory(converterFactory)
            .addCallAdapterFactory(adapter)
            .build()
            .create(TestApi::class.java)
}