package com.example.interviewtest.application.builder

import com.example.interviewtest.api.TestApi
import com.example.interviewtest.application.builder.modules.AppContextModule
import com.example.interviewtest.application.builder.modules.NetworkModule
import com.example.interviewtest.application.builder.modules.api.TestApiModule
import com.example.interviewtest.persistence.AppDatabase
import com.example.interviewtest.utils.PropertiesReader
import com.example.interviewtest.utils.rx.RxSchedulers
import dagger.Component

@AppScope
@Component(modules = [AppContextModule::class, NetworkModule::class, TestApiModule::class])
interface AppComponent {
    fun appDatabase() : AppDatabase
    fun testApi() : TestApi
    fun propertiesReader() : PropertiesReader
    fun rxSchedulers(): RxSchedulers
}