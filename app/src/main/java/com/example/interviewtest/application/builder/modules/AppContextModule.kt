package com.example.interviewtest.application.builder.modules

import android.content.Context
import com.example.interviewtest.application.builder.AppScope
import com.example.interviewtest.persistence.DatabaseBuilder
import com.example.interviewtest.utils.PropertiesReader
import com.example.interviewtest.utils.rx.RxSchedulers
import com.example.interviewtest.utils.rx.RxSchedulersImp
import dagger.Module
import dagger.Provides

@Module
class AppContextModule(private val context: Context) {

    @AppScope
    @Provides
    fun provideAppContext() = context

    @AppScope
    @Provides
    fun provideAppDatabase() = DatabaseBuilder.getInstance(context)

    @AppScope
    @Provides
    fun providePropertiesReader() = PropertiesReader(context)

    @AppScope
    @Provides
    fun provideRxSchedulers(): RxSchedulers = RxSchedulersImp()
}