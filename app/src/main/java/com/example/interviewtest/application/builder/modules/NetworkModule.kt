package com.example.interviewtest.application.builder.modules

import com.example.interviewtest.BuildConfig
import com.example.interviewtest.application.builder.AppScope
import com.example.interviewtest.utils.rx.RxSchedulersImp
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class NetworkModule {

    @AppScope
    @Provides
    fun provideClient(logger: HttpLoggingInterceptor) : OkHttpClient{
        return OkHttpClient.Builder()
            .connectTimeout(30000, TimeUnit.MILLISECONDS)
            .readTimeout(30000, TimeUnit.MILLISECONDS)
            .writeTimeout(30000, TimeUnit.MILLISECONDS)
            .addNetworkInterceptor(logger)
            .build()
    }

    @AppScope
    @Provides
    fun provideLogger(): HttpLoggingInterceptor{
        val logger = HttpLoggingInterceptor()
        if(BuildConfig.DEBUG){
            logger.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logger.level = HttpLoggingInterceptor.Level.NONE
        }
        return logger
    }

    @AppScope
    @Provides
    fun provideRxAdapter(): RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.createWithScheduler(RxSchedulersImp.INTERNET_SCHEDULERS)

    @AppScope
    @Provides
    fun provideConverterFactory(): GsonConverterFactory =
        GsonConverterFactory.create()

}