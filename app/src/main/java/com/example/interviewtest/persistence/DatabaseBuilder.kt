package com.example.interviewtest.persistence

import android.content.Context
import androidx.room.Room

object DatabaseBuilder {
    private var instance : AppDatabase? = null
    fun getInstance(context: Context) : AppDatabase {
        if (instance == null) {
            synchronized(AppDatabase::class){
                instance = buildDatabase(context)
            }
        }
        return instance!!
    }

    private fun buildDatabase(context: Context) =
        Room.databaseBuilder(
            context.applicationContext,
            AppDatabase::class.java,
            "test-DB"
        ).build()
}