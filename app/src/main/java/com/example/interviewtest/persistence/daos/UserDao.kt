package com.example.interviewtest.persistence.daos

import androidx.room.*
import com.example.interviewtest.persistence.entities.User
import io.reactivex.Maybe

@Dao
interface UserDao {

    @Query("SELECT * FROM user")
    suspend fun getAll():List<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User)

    @Transaction
    @Query("DELETE FROM user")
    fun deleteUser();
}