package com.example.interviewtest.api.dto

data class UsersListResponse(val resources: List<UserModel>)