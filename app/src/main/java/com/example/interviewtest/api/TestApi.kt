package com.example.interviewtest.api

import com.example.interviewtest.api.dto.LoginRequest
import com.example.interviewtest.api.dto.UsersListResponse
import com.example.interviewtest.persistence.entities.User
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface TestApi {

    @POST("login")
    @Headers("Content-Type: application/json")
    fun doLogin(@Body request: LoginRequest): Observable<User>

    @GET("users")
    fun getUsers(): Observable<UsersListResponse>
}