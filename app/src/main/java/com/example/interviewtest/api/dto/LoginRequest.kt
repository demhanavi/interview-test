package com.example.interviewtest.api.dto

data class LoginRequest(val email: String, val password: String)