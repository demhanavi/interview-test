package com.example.interviewtest.utils

import android.content.Context
import java.util.*

class PropertiesReader(private val context: Context) {

    private val properties: Properties = Properties()
    companion object {
        private const val FILE_NAME = "application.properties"
    }

    init {
        loadProperties()
    }

    private fun loadProperties(){
        val inputStream = context.assets.open(FILE_NAME)
        properties.load(inputStream)
    }

    private fun getProperty(name: String): Any = properties.getProperty(name)

    fun getBaseUrl():String = getProperty("app.base.url").toString()
}